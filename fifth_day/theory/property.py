class Point:
    def __init__(self, x, y):
        self._x = x
        self._y = y

    def get_x(self):
        return self._x

    def set_x(self, value):
        self._x = value

    def get_y(self):
        return self._y

    def set_y(self, value):
        self._y = value


class PrettyPoint:
    def __init__(self, x, y):
        self.x = x
        self.y = y

# property(fget=None, fset=None, fdel=None, doc=None)


# class Circle:
#     def __init__(self, radius):
#         self._radius = radius
#
#     def _get_radius(self):
#         print("Get radius")
#         return self._radius
#
#     def _set_radius(self, value):
#         print("Set radius")
#         self._radius = value
#
#     def _del_radius(self):
#         print("Delete radius")
#         del self._radius
#
#     radius = property(
#         fget=_get_radius,
#         fset=_set_radius,
#         fdel=_del_radius,
#         doc="The radius property."
#     )


class Circle:
    def __init__(self, radius):
        self._radius = radius

    @property
    def radius(self):
        """The radius property."""
        print("Get radius")
        return self._radius

    @radius.setter
    def radius(self, value):
        print("Set radius")
        self._radius = value

    @radius.deleter
    def radius(self):
        print("Delete radius")
        del self._radius
